package com.apegroup.takeout.event;

public interface BasketChangeListener {

	public void onUpdateItemChange(BasketChangeEvent event);
}
