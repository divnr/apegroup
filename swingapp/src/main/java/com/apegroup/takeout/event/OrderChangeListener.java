package com.apegroup.takeout.event;

public interface OrderChangeListener {

	public void onOrderChange(OrderChangeEvent event);
}
