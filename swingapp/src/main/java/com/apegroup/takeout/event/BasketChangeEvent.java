package com.apegroup.takeout.event;

import com.apegroup.takeout.model.Basket;

public class BasketChangeEvent {

	private Basket source;

	public Basket getSource() {
		return source;
	}

	public void setSource(Basket source) {
		this.source = source;
	}
}
