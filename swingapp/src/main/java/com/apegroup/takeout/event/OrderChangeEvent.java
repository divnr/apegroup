package com.apegroup.takeout.event;

import com.apegroup.takeout.model.Order;

public class OrderChangeEvent {

	private Order source;

	public Order getSource() {
		return source;
	}

	public void setSource(Order source) {
		this.source = source;
	}
}
