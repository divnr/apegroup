package com.apegroup.takeout.ui.panel;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.manager.MenuManager;
import com.apegroup.takeout.model.MenuItem;
import com.apegroup.takeout.ui.button.AddToBasketButton;
import com.apegroup.takeout.ui.button.CheckoutButton;
import com.apegroup.takeout.ui.button.RemoveFromBasketButton;
import com.apegroup.takeout.ui.label.ItemCounterLabel;

public class MenuPanel extends JPanel {

	private static final long serialVersionUID = 12347983274598374L;

	public static final String PANEL_NAME = "Takeout Menu";
	
	
	public void init() {
		
		JPanel menuContents = new JPanel(new GridLayout(0,1)) ;
		
		JScrollPane menu = new JScrollPane(menuContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		menu.setPreferredSize(new Dimension(260, 300));
		
		for (MenuItem currentMenuItem : MenuManager.getInstance().getMenu().getMenuItems()) {

			JPanel currentMenuBlock = createMenuItemPanel(currentMenuItem);

			menuContents.add(currentMenuBlock);
		}

		this.add(menu);
		
		CheckoutButton checkoutButton = new CheckoutButton("CHECKOUT");
		
		BasketManager.getInstance().addBasketChangeListener(checkoutButton);
		
		checkoutButton.addActionListener(checkoutButton);
		
		checkoutButton.setPreferredSize(new Dimension(150,50));
		
		this.add(checkoutButton);
	}

	public JPanel createMenuItemPanel(final MenuItem menuItem) {

		JPanel menuItemPanel = new JPanel();
		menuItemPanel.setPreferredSize(new Dimension(230, 60));

		GridLayout layout = new GridLayout(2, 2);

		menuItemPanel.setLayout(layout);

		JLabel menuItemLabel = new JLabel(menuItem.getName());

		menuItemPanel.add(menuItemLabel);

		menuItemPanel.add(new ItemCounterLabel(menuItem));

		AddToBasketButton addButton = new AddToBasketButton(menuItem);
		addButton.addActionListener(addButton);
		menuItemPanel.add(addButton);

		RemoveFromBasketButton removeButton = new RemoveFromBasketButton(menuItem);
		
		removeButton.addActionListener(removeButton);
		menuItemPanel.add(removeButton);
		
		return menuItemPanel;
	}
}
