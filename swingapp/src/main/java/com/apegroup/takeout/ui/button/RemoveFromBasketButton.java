package com.apegroup.takeout.ui.button;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.model.MenuItem;

public class RemoveFromBasketButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 13453489348938493L;

	private MenuItem menuItem ;
	
	public RemoveFromBasketButton(MenuItem menuItem) {
		super("-");
		this.menuItem = menuItem;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		BasketItem removedItem = new BasketItem();
		
		removedItem.setCorrespondingMenuItem(menuItem);
		removedItem.setQuantity(-1);
		
		BasketManager.getInstance().removeItemFromBasket(removedItem);
	}
	
	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}
}
