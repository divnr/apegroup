package com.apegroup.takeout.ui.button;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.apegroup.takeout.manager.OrderManager;
import com.apegroup.takeout.ui.OrderApplication;
import com.apegroup.takeout.ui.panel.ReceiptPanel;

public class ConfirmButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 1743298475987423L;

	public ConfirmButton() {
		super("CONFIRM");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		OrderManager.getInstance().placeOrder();
		
		CardLayout cardLayout = (CardLayout) OrderApplication.cards.getLayout();
		
		cardLayout.show(OrderApplication.cards, ReceiptPanel.PANEL_NAME);
	}

}
