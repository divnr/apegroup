package com.apegroup.takeout.ui.panel;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.apegroup.takeout.event.OrderChangeEvent;
import com.apegroup.takeout.event.OrderChangeListener;
import com.apegroup.takeout.manager.OrderManager;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.model.OrderItem;
import com.apegroup.takeout.model.OrderStatus;
import com.apegroup.takeout.ui.button.MenuButton;

public class ReceiptPanel extends JPanel implements OrderChangeListener {

	private static final long serialVersionUID = 1093274203740203L;

	public static final String PANEL_NAME = "Receipt";
	
	private JPanel receiptSummary;
	
	public void init() {
		
		receiptSummary = new JPanel(new GridLayout(0,2));
		
		MenuButton menuButton = new MenuButton();
		
		menuButton.addActionListener(menuButton);
		
		menuButton.setPreferredSize(new Dimension(150,50));
		
		this.add(receiptSummary);
		this.add(menuButton);
		
		OrderManager.getInstance().addOrderChangeListener(this);
	}

	@Override
	public void onOrderChange(OrderChangeEvent event) {
		
		receiptSummary.removeAll();
		
		Order lastOrder = OrderManager.getInstance().getLastOrder();
		
		if(lastOrder.getStatus()==OrderStatus.SUCCESSFUL_ORDER)  { 
			printReceipt(lastOrder);
		}
		else if(lastOrder.getStatus()==OrderStatus.BAD_INPUT_PARAMETER)  {
			receiptSummary.add(new JLabel("Incorrect Input Parameter"));
		}
		else if(lastOrder.getStatus()==OrderStatus.STOCK_EMPTY) {
			receiptSummary.add(new JLabel("Stock Empty"));
		}
		
	}
	
	public void printReceipt(Order lastOrder) {

		receiptSummary.add(new JLabel("Delivery Date"));
		receiptSummary.add(new JLabel(String.valueOf(
				DateFormat.getDateInstance().format(new Date(lastOrder.getDeliveryDate())))));
		
		receiptSummary.add(new JLabel("Customer"));
		receiptSummary.add(new JLabel(lastOrder.getCustomer().getName()));
		
		receiptSummary.add(new JLabel("Address"));
		receiptSummary.add(new JLabel(lastOrder.getCustomer().getAddress()));
		
		for(OrderItem currentItem : lastOrder.getOrderItems()) {
			receiptSummary.add(new JLabel(String.valueOf(currentItem.getQuantity())));
			receiptSummary.add(new JLabel(currentItem.getCorrespondingMenuItem().getName()));
		}
		
		receiptSummary.add(new JLabel("Total Charge"));
		receiptSummary.add(new JLabel(String.valueOf(lastOrder.getTotalCost())));
	}
}

