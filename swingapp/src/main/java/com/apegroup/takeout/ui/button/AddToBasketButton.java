package com.apegroup.takeout.ui.button;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.model.MenuItem;

public class AddToBasketButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 13453489348938493L;
	
	private MenuItem menuItem ;
	
	public AddToBasketButton(MenuItem menuItem) {
		super("+");
		this.menuItem = menuItem;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		BasketItem addedItem = new BasketItem();
		
		addedItem.setCorrespondingMenuItem(menuItem);
		addedItem.setQuantity(1);
		
		BasketManager.getInstance().addItemToBasket(addedItem);
	}
	
	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}
}
