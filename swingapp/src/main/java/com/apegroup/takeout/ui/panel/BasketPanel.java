package com.apegroup.takeout.ui.panel;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.BasketItem;

public class BasketPanel extends JPanel {

	private static final long serialVersionUID = 1327894923749L;

	public void refreshContentsOfBasket(Basket basket) {
		this.removeAll();
		
		for(BasketItem currentItem:basket.getBasketItems()) {
			this.add(createBasketItemPanel(currentItem));
		}
	}
	
	public JPanel createBasketItemPanel(BasketItem basketItem) {

		JPanel basketItemPanel = new JPanel();
		basketItemPanel.setPreferredSize(new Dimension(250, 60));

		GridLayout layout = new GridLayout(1, 2);
		
		basketItemPanel.setLayout(layout);
		
		JLabel itemLabel = new JLabel(basketItem.getCorrespondingMenuItem().getName());
		JLabel quantityLabel = new JLabel(String.valueOf(basketItem.getQuantity()));
		
		basketItemPanel.add(itemLabel);
		basketItemPanel.add(quantityLabel);
		
		return basketItemPanel;
	}
}
