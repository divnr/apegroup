package com.apegroup.takeout.ui.panel;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.apegroup.takeout.event.BasketChangeEvent;
import com.apegroup.takeout.event.BasketChangeListener;
import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.ui.button.ConfirmButton;
import com.apegroup.takeout.ui.button.MenuButton;

public class ConfirmationPanel extends JPanel implements BasketChangeListener {

	private static final long serialVersionUID = 142304902384902L;

	public static final String PANEL_NAME = "Confirmation";
	
	private BasketPanel basketPanel;
	
	public void init() {
		
		JPanel confirmContents = new JPanel(new GridLayout(0,1)) ;
		
		JScrollPane confirm = new JScrollPane(confirmContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		confirm.setPreferredSize(new Dimension(260, 300));
		
		basketPanel = new BasketPanel();
		basketPanel.setPreferredSize(new Dimension(250, 300));
		
		basketPanel.refreshContentsOfBasket(BasketManager.getInstance().getBasket());
		
		confirmContents.add(basketPanel);
		this.add(confirm);
		
		JPanel buttonPanel = new JPanel(new GridLayout(1,2));
		
		MenuButton menuButton = new MenuButton();
		menuButton.addActionListener(menuButton);
		
		ConfirmButton confirmButton = new ConfirmButton();
		confirmButton.addActionListener(confirmButton);
		
		menuButton.setPreferredSize(new Dimension(130,50));
		confirmButton.setPreferredSize(new Dimension(130,50));
		
		buttonPanel.add(menuButton);
		buttonPanel.add(confirmButton);
		
		this.add(buttonPanel);
		
		BasketManager.getInstance().addBasketChangeListener(this);
	}
	
	@Override
	public void onUpdateItemChange(BasketChangeEvent event) {
		
		basketPanel.refreshContentsOfBasket(BasketManager.getInstance().getBasket());
	}
}
