package com.apegroup.takeout.ui.label;

import javax.swing.JLabel;

import com.apegroup.takeout.event.BasketChangeEvent;
import com.apegroup.takeout.event.BasketChangeListener;
import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.model.MenuItem;

public class ItemCounterLabel extends JLabel implements BasketChangeListener {

	private static final long serialVersionUID = 190823748932749379L;

	private MenuItem menuItem ;
	
	public ItemCounterLabel(MenuItem menuItem) {
		super("0");
		this.menuItem = menuItem;
		
		BasketManager.getInstance().addBasketChangeListener(this);
	}
	
	@Override
	public void onUpdateItemChange(BasketChangeEvent event) {
		BasketItem basketItem = BasketManager.getInstance().getBasketItem(menuItem.getId());
		
		if(basketItem != null)  {
			this.setText(String.valueOf(basketItem.getQuantity()));
		}
		else {
			this.setText("0");
		}
	}
}
