package com.apegroup.takeout.ui.button;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.apegroup.takeout.event.BasketChangeEvent;
import com.apegroup.takeout.event.BasketChangeListener;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.ui.OrderApplication;
import com.apegroup.takeout.ui.panel.ConfirmationPanel;

public class CheckoutButton extends JButton implements BasketChangeListener, ActionListener {

	private static final long serialVersionUID = 1903457809348509348L;

	public CheckoutButton(String text) {
		super(text);
		this.setEnabled(false);
	}
	

	@Override
	public void onUpdateItemChange(BasketChangeEvent event) {
		
		Basket basket = event.getSource();
		
		if(basket.getTotalItems()>0) {
			this.setEnabled(true);
		}
		else {
			this.setEnabled(false);
		}
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		CardLayout cardLayout = (CardLayout) OrderApplication.cards.getLayout();
		
		cardLayout.show(OrderApplication.cards, ConfirmationPanel.PANEL_NAME);
	}
}
