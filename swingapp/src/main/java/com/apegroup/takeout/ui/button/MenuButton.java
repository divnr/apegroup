package com.apegroup.takeout.ui.button;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.apegroup.takeout.ui.OrderApplication;
import com.apegroup.takeout.ui.panel.MenuPanel;

public class MenuButton extends JButton implements ActionListener {

	private static final long serialVersionUID = 15345735738953495L;

	public MenuButton() {
		super("BACK TO MENU");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		CardLayout cardLayout = (CardLayout) OrderApplication.cards.getLayout();
		
		cardLayout.show(OrderApplication.cards, MenuPanel.PANEL_NAME);
	}

}
