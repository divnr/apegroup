package com.apegroup.takeout.ui;

import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.apegroup.takeout.manager.BasketManager;
import com.apegroup.takeout.manager.ClientManager;
import com.apegroup.takeout.manager.MenuManager;
import com.apegroup.takeout.manager.OrderManager;
import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.service.exceptions.ClientException;
import com.apegroup.takeout.ui.panel.ConfirmationPanel;
import com.apegroup.takeout.ui.panel.MenuPanel;
import com.apegroup.takeout.ui.panel.ReceiptPanel;

public class OrderApplication {

	private static Customer customer = new Customer("Adam", "67 Chamber road");

	public static JPanel cards;
	
	public static void initManagers() {
		
		try {
			ClientManager.getInstance().init(com.apegroup.pizzeria.rest.client.PizzeriaOrderClient.class);
		} catch (ClientException e) {
			e.printStackTrace();
		}

		BasketManager.getInstance().init(customer);
		
		MenuManager.getInstance().init(customer);
		
		OrderManager.getInstance();
	}
	
	public static void createAndShowGUI() {

		JFrame frame = new JFrame("Pizzeria Application");

		frame.setPreferredSize(new Dimension(285, 400));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		CardLayout cardLayout = new CardLayout();
		
		cards = new JPanel(cardLayout);

		cards.setPreferredSize(new Dimension(250, 400));

		MenuPanel menuPanel = new MenuPanel();
		
		menuPanel.init();

		cards.add(menuPanel,MenuPanel.PANEL_NAME);

		ConfirmationPanel confirmPanel = new ConfirmationPanel();
		
		confirmPanel.init();
		
		cards.add(confirmPanel,ConfirmationPanel.PANEL_NAME);
		
		ReceiptPanel receiptPanel = new ReceiptPanel();
		
		receiptPanel.init();
		
		cards.add(receiptPanel,ReceiptPanel.PANEL_NAME);
		
		frame.getContentPane().add(cards);

		cardLayout.show(cards, MenuPanel.PANEL_NAME);
		
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		
		initManagers();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
