package com.apegroup.takeout.manager;

import com.apegroup.takeout.service.client.IOrderClient;
import com.apegroup.takeout.service.exceptions.ClientException;

public class ClientManager {

	private static ClientManager INSTANCE;
	
	private IOrderClient client;
	
	public static ClientManager getInstance() throws ClientException {
		
		synchronized(ClientManager.class){
	            if(INSTANCE == null){
	                INSTANCE = new ClientManager();
	            }
        }

	    return INSTANCE;
	}

	public void init(Class clientClass) throws ClientException {
		try {
			INSTANCE.client = (IOrderClient) clientClass.newInstance();
		} catch (InstantiationException e) {
			throw new ClientException(e);
		} catch (IllegalAccessException e) {
			throw new ClientException(e);
		}
	}
	
	public IOrderClient getClient() {
		return client;
	}

	public void setClient(IOrderClient client) {
		this.client = client;
	}
}
