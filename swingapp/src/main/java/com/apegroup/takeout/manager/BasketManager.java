package com.apegroup.takeout.manager;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.apegroup.takeout.event.BasketChangeEvent;
import com.apegroup.takeout.event.BasketChangeListener;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.model.Customer;

public class BasketManager {

	private static BasketManager INSTANCE ;
	
	private Customer customer;
	
	private Basket basket;
	
	private List<BasketChangeListener> basketChangeListeners =
			new ArrayList<BasketChangeListener>();

	public static BasketManager getInstance() {
		synchronized(BasketManager.class){
	            if(INSTANCE == null){
	                INSTANCE = new BasketManager();
	                INSTANCE.basket = new Basket();
	            }
        }

	    return INSTANCE;
	}
	
	public void init(Customer customer) {
		this.customer = customer;
		basket.setCustomer(customer);
	}
	
	public void addItemToBasket(BasketItem itemToAdd) {
		updateBasketItem(itemToAdd);
		fireOnUpdateItemBasketChangeEvent();
	}
	
	public void removeItemFromBasket(BasketItem itemToAdd) {
		updateBasketItem(itemToAdd);
		fireOnUpdateItemBasketChangeEvent();
	}
	
	public void updateBasketItem(BasketItem itemToAdd) {
		if(this.basket.getOrderedBasketItems() == null) {
			this.basket.setOrderedBasketItems(new Hashtable<String,BasketItem>());
		}
		
		String menuId = itemToAdd.getCorrespondingMenuItem().getId();
		
		if(this.basket.getOrderedBasketItems().containsKey(menuId)) {
			BasketItem matchedItem = this.basket.getOrderedBasketItems().get(menuId); 
			
			int updatedQuantity = itemToAdd.getQuantity() + matchedItem.getQuantity();
			
			if(updatedQuantity < 0) { 
				updatedQuantity = 0 ;
			}
			
			matchedItem.setQuantity(updatedQuantity);
		}
		else if(itemToAdd.getQuantity()>0) {
			this.basket.getOrderedBasketItems().put(itemToAdd.getCorrespondingMenuItem().getId(),
					itemToAdd);
		}
	}
	
	public void resetBasket() {
		this.basket = new Basket(this.customer) ;
		fireOnUpdateItemBasketChangeEvent();
	}
	
	public Basket getBasket() {
		return basket;
	}

	public void setBasket(Basket basket) {
		this.basket = basket;
	}

	public BasketItem getBasketItem(String id) {
		return basket.getOrderedBasketItems().get(id);
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void addBasketChangeListener(BasketChangeListener changeListener) {
		this.basketChangeListeners.add(changeListener);
	}
	
	public void removeBasketChangeListener(BasketChangeListener changeListener) {
		this.basketChangeListeners.remove(changeListener);
	}
	
	private void fireOnUpdateItemBasketChangeEvent() {
		BasketChangeEvent event = new BasketChangeEvent();
		
		event.setSource(this.basket);
		
		for(BasketChangeListener currentListener : basketChangeListeners) {
			currentListener.onUpdateItemChange(event);
		}
	}
}
