package com.apegroup.takeout.manager;

import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.model.Menu;
import com.apegroup.takeout.service.exceptions.ClientException;
import com.apegroup.takeout.service.exceptions.MenuException;

public class MenuManager {

	private static MenuManager INSTANCE;
	
	private Menu menu;
	
	public static MenuManager getInstance() {
		synchronized(MenuManager.class){
	            if(INSTANCE == null){
	                INSTANCE = new MenuManager();
	            }
        }

	    return INSTANCE;
	}
	
	
	public void init(Customer customer) {
		try {
			menu = ClientManager.getInstance().getClient().constructMenu(customer);
		} catch (MenuException | ClientException e) {
			e.printStackTrace();
		}
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
}
