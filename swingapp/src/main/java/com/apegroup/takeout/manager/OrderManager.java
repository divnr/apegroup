package com.apegroup.takeout.manager;

import java.util.ArrayList;
import java.util.List;

import com.apegroup.takeout.event.BasketChangeEvent;
import com.apegroup.takeout.event.BasketChangeListener;
import com.apegroup.takeout.event.OrderChangeEvent;
import com.apegroup.takeout.event.OrderChangeListener;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.service.exceptions.ClientException;
import com.apegroup.takeout.service.exceptions.OrderException;

public class OrderManager {

	private static OrderManager INSTANCE;
	
	private Order lastOrder;
	
	private List<OrderChangeListener> orderChangeListeners =
			new ArrayList<OrderChangeListener>();
	
	public static OrderManager getInstance() {
		synchronized(OrderManager.class){
	            if(INSTANCE == null){
	                INSTANCE = new OrderManager();
	            }
        }

	    return INSTANCE;
	}
	
	public void placeOrder() {
		
		Basket basket = BasketManager.getInstance().getBasket();
		
		try {
			lastOrder = ClientManager.getInstance().getClient().placeOrder(basket);
		} catch (OrderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BasketManager.getInstance().resetBasket();
		
		fireOnOrderChangeEvent();
	}
	
	
	
	public Order getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(Order lastOrder) {
		this.lastOrder = lastOrder;
	}

	public void addOrderChangeListener(OrderChangeListener changeListener) {
		this.orderChangeListeners.add(changeListener);
	}
	
	public void removeOrderChangeListener(OrderChangeListener changeListener) {
		this.orderChangeListeners.remove(changeListener);
	}
	
	private void fireOnOrderChangeEvent() {
		OrderChangeEvent event = new OrderChangeEvent();
		
		event.setSource(this.lastOrder);
		
		for(OrderChangeListener currentListener : orderChangeListeners) {
			currentListener.onOrderChange(event);
		}
	}
}
