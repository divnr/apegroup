package com.apegroup.takeout.conversion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.apegroup.pizzeria.rest.model.Base;
import com.apegroup.pizzeria.rest.model.OrderRequest;
import com.apegroup.pizzeria.rest.model.OrderResponse;
import com.apegroup.pizzeria.rest.model.Pizza;
import com.apegroup.pizzeria.rest.model.PizzaOrderItem;
import com.apegroup.pizzeria.rest.model.Topping;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.model.Menu;
import com.apegroup.takeout.model.MenuItem;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.model.OrderItem;

public class PizzeriaConversionUtil {

	public static OrderRequest convertFromBasketToOrderRequest(Basket basket) {

		OrderRequest orderRequest = new OrderRequest();

		orderRequest.setName(basket.getCustomer().getName());
		orderRequest.setAddress(basket.getCustomer().getAddress());

		List<Pizza> requestedPizzas = new ArrayList<Pizza>();

		for (int counter = 0; counter < basket.getBasketItems().size(); counter++) {

			BasketItem currentOrderItem = (BasketItem) basket.getBasketItems()
					.get(counter);

			for (int quantity = 0; quantity < currentOrderItem.getQuantity(); quantity++) {
				Pizza currentRequestedPizza = new Pizza();
				currentRequestedPizza.setId(Integer.parseInt(currentOrderItem
						.getCorrespondingMenuItem().getId()));
				requestedPizzas.add(currentRequestedPizza);
			}
		}

		orderRequest.setPizzas(requestedPizzas);

		return orderRequest;
	}

	public static Order convertOrderResponseToTakeoutOrder(
			OrderResponse orderResponse, Status responseStatus) {

		Order orderToReturn = new Order();

		orderToReturn.setId(orderResponse.getId());

		for (PizzaOrderItem currentPizzaOrderItem : orderResponse
				.getPizzaOrders()) {

			MenuItem convertedMenuItem = convertPizzaToMenuItem(currentPizzaOrderItem
					.getPizza());

			OrderItem currentOrderItem = new OrderItem();

			currentOrderItem.setCorrespondingMenuItem(convertedMenuItem);
			currentOrderItem.setQuantity(currentPizzaOrderItem.getAmount());

			orderToReturn.addOrderItem(currentOrderItem);
		}

		orderToReturn.setTotalItems(orderResponse.getQuantity());
		orderToReturn.setTotalCost(orderResponse.getTotal());

		Customer customer = new Customer();

		customer.setName(orderResponse.getName());
		customer.setAddress(orderResponse.getAddress());

		orderToReturn.setCustomer(customer);

		orderToReturn.setDeliveryDate(orderResponse.getDeliveryDate());

		return orderToReturn;
	}

	public static Menu convertPizzaListToMenu(List<Pizza> pizzas) {

		Menu menuToReturn = new Menu();

		List<MenuItem> menuItems = new ArrayList<MenuItem>();

		Iterator<Pizza> pizzaIter = pizzas.iterator();

		while (pizzaIter.hasNext()) {
			menuItems.add(convertPizzaToMenuItem((Pizza) pizzaIter.next()));
		}

		menuToReturn.setMenuItems(menuItems);

		return menuToReturn;
	}

	public static MenuItem convertPizzaToMenuItem(Pizza pizza) {

		MenuItem pizzaItemToReturn = new MenuItem();

		pizzaItemToReturn.setId(String.valueOf(pizza.getId()));
		pizzaItemToReturn.setName(pizza.getName());
		pizzaItemToReturn.setPrice(pizza.getPrice());

		Base pizzaBase = pizza.getBase();
		MenuItem baseItem = new MenuItem();
		baseItem.setId(String.valueOf(pizzaBase.getId()));
		baseItem.setName(pizzaBase.getName());
		pizzaItemToReturn.addChildItem(baseItem);

		Iterator<Topping> toppingIter = pizza.getToppings().iterator();

		while (toppingIter.hasNext()) {
			Topping currentTopping = (Topping) toppingIter.next();

			MenuItem toppingItem = new MenuItem();

			toppingItem.setId(String.valueOf(currentTopping.getId()));
			toppingItem.setName(currentTopping.getName());

			pizzaItemToReturn.addChildItem(toppingItem);
		}

		return pizzaItemToReturn;
	}
}
