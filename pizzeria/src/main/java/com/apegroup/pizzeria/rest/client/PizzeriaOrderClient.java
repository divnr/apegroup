package com.apegroup.pizzeria.rest.client;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import com.apegroup.pizzeria.rest.model.OrderRequest;
import com.apegroup.pizzeria.rest.model.OrderResponse;
import com.apegroup.pizzeria.rest.model.Pizza;
import com.apegroup.takeout.conversion.PizzeriaConversionUtil;
import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.model.Menu;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.model.OrderStatus;
import com.apegroup.takeout.service.client.IOrderClient;
import com.apegroup.takeout.service.exceptions.MenuException;
import com.apegroup.takeout.service.exceptions.OrderException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.logging.Logger;
import org.jboss.resteasy.util.GenericType;

public class PizzeriaOrderClient implements IOrderClient {

	private static final String PIZZERIA_MENUS_ENDPOINT =
            "http://ape-pizzeria.cloudfoundry.com/pizzas"; 

	private static final String PIZZERIA_ORDERS_ENDPOINT =
            "http://ape-pizzeria.cloudfoundry.com/pizzaorders?param=val"; 
	
	@Override
	public Menu constructMenu(Customer customer) throws MenuException {
		
		ClientRequest req = new ClientRequest(PIZZERIA_MENUS_ENDPOINT);
		req.pathParameter("param", "val");
		
		ClientResponse<List<Pizza>> res = null;
		
		try {
			res = req.get(new GenericType<List<Pizza>>(){});
		} catch (Exception e) {
			throw new MenuException(e);
		}
		
		// convert to "takeout" generic objects
		Menu menuToReturn = PizzeriaConversionUtil.convertPizzaListToMenu(res.getEntity());
		
		return menuToReturn;
	}

	@Override
	public Order placeOrder(Basket basket) throws OrderException {
		
		ClientRequest req = new ClientRequest(PIZZERIA_ORDERS_ENDPOINT);
		req.pathParameter("param", "val");
		
		// convert to Pizzeria order from "Takeout" order
		OrderRequest orderRequest = 
				PizzeriaConversionUtil.convertFromBasketToOrderRequest(basket);
		
		ObjectMapper mapper = new ObjectMapper();
		
		String orderRequestStr = null;
		
		try {
			orderRequestStr = mapper.writeValueAsString(orderRequest);
		} catch (JsonGenerationException jsonGenEx) {
			throw new OrderException("An exception was thrown whilst attempting to place an order.",jsonGenEx);
		} catch (JsonMappingException jsonMapEx) {
			throw new OrderException("An exception was thrown whilst attempting to place an order.",jsonMapEx);
		} catch (IOException ioEx) {
			throw new OrderException("An exception was thrown whilst attempting to place an order.",ioEx);
		}
		
		req.body("application/json", orderRequestStr);
		
		ClientResponse<OrderResponse> res = null;
		
		try {
			res = req.post(OrderResponse.class);
		} catch (Exception e) {
			throw new OrderException(e);	
		}
		
		Order orderToReturn = null;
		
		if(res.getResponseStatus() == Status.CREATED) {
			orderToReturn = PizzeriaConversionUtil.convertOrderResponseToTakeoutOrder(res.getEntity(), res.getResponseStatus());
			orderToReturn.setStatus(OrderStatus.SUCCESSFUL_ORDER);
		}
		else if(res.getResponseStatus() == Status.NOT_FOUND) {
			orderToReturn = new Order();
			orderToReturn.setStatus(OrderStatus.BAD_INPUT_PARAMETER);
			return orderToReturn;
		}
		else if(res.getResponseStatus() == Status.BAD_REQUEST) {
			orderToReturn = new Order();
			orderToReturn.setStatus(OrderStatus.STOCK_EMPTY);
			return orderToReturn;
		}
		else if(res.getResponseStatus() == Status.INTERNAL_SERVER_ERROR) {
			orderToReturn = new Order();
			orderToReturn.setStatus(OrderStatus.INTERNAL_SERVER_ERROR);
			return orderToReturn;
		}
		
		return orderToReturn;
	}
	
	
}