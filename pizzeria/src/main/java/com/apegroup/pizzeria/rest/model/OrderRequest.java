package com.apegroup.pizzeria.rest.model;

import java.io.Serializable;
import java.util.List;

public class OrderRequest implements Serializable {

	private static final long serialVersionUID = 7576472556423476147L;
	
	private String name ;
	private String address;
	private List<Pizza> pizzas;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
}
