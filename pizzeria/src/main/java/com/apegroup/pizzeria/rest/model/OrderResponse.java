package com.apegroup.pizzeria.rest.model;

import java.io.Serializable;
import java.util.List;


/**
 * {"address":"24 Kocksgatan","deliveryDate":1370346133043,"id":849,"name":"Adam","pizzaOrders":[
 * {"amount":1,"pizza":{"base":{"id":1,"name":"Thin Crust"},"id":1,"name":"Ape Speciale","price":11.00,"toppings":[{"id":1,"name":"Cheese"}]}},
 * {"amount":1,"pizza":{"base":{"id":2,"name":"Thick Crust"},"id":3,"name":"Caprichosa","price":98.00,"toppings":[{"id":4,"name":"Mushroms"},{"id":2,"name":"Ham"},{"id":1,"name":"Cheese"},{"id":3,"name":"Fresh tomato"}]}}],"quantity":2,"total":109.00}

 * 
 * 
 * 
 * @author adambell
 *
 */

public class OrderResponse implements Serializable {

	private static final long serialVersionUID = 7576472556423476147L;
	
	private int id;
	
	private String name;
	private String address;
	
	private double total;
	
	private int quantity;
	
	private long deliveryDate;
	
	private List<PizzaOrderItem> pizzaOrders;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<PizzaOrderItem> getPizzaOrders() {
		return pizzaOrders;
	}

	public void setPizzaOrders(List<PizzaOrderItem> pizzaOrders) {
		this.pizzaOrders = pizzaOrders;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
