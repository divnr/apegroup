package com.apegroup.pizzeria.rest.model;

import java.io.Serializable;

public class PizzaOrderItem implements Serializable {

	private static final long serialVersionUID = 7576476756423476147L;
	
	private int amount; 
	
	private Pizza pizza;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}
}
