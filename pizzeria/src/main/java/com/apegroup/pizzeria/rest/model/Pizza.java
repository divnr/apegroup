package com.apegroup.pizzeria.rest.model;

import java.io.Serializable;
import java.util.List;

public class Pizza implements Serializable {

	private static final long serialVersionUID = 7596472556423476147L;
	
	// ,"id":1,"name":"Ape Speciale","price":11.00,version:1
	
	private int id;
	private String name;
	private double price;
	private int version;
	
	private List<Topping> toppings;
	
	private Base base;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public List<Topping> getToppings() {
		return toppings;
	}

	public void setToppings(List<Topping> toppings) {
		this.toppings = toppings;
	}

	public Base getBase() {
		return base;
	}

	public void setBase(Base base) {
		this.base = base;
	}
}
