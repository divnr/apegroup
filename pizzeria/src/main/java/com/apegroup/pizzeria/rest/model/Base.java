package com.apegroup.pizzeria.rest.model;

import java.io.Serializable;

public class Base implements Serializable {

	private static final long serialVersionUID = 7576472556423476147L;
	
	// {"base":{"id":1,"name":"Thin Crust","version":0}
	
	private int id;
	private String name;
	private int version;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
}
