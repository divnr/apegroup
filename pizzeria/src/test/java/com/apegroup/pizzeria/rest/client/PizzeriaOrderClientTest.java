package com.apegroup.pizzeria.rest.client;

import static org.junit.Assert.*;

import org.junit.Test;

import com.apegroup.takeout.service.client.IOrderClient;

import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.model.Menu;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.model.BasketItem;
import com.apegroup.takeout.service.exceptions.MenuException;
import com.apegroup.takeout.service.exceptions.OrderException;

public class PizzeriaOrderClientTest {

	private static final String CUSTOMER_NAME = "Adam";
	private static final String CUSTOMER_ADDRESS = "24 Kocksgatan";	
	
	private IOrderClient client = new PizzeriaOrderClient();
	
	private static Menu menu;
	
	@Test
	public void testContructMenu() {
		
		Customer takeOutCustomer = new Customer();
		
		Menu returnedMenu = null;
		
		try {
			returnedMenu = client.constructMenu(takeOutCustomer);
		} catch (MenuException menuEx) {
			fail("Exception thrown constucting menu : " + menuEx);
		}
		
		boolean emptyOrNullMenu = returnedMenu == null || returnedMenu.getMenuItems() == null
						|| returnedMenu.getMenuItems().isEmpty();
		
		assertFalse("Returned menu was empty or null", emptyOrNullMenu);
		
		menu = returnedMenu ;
	}

	@Test
	public void testPlaceOrder() {
		
		Customer orderCustomer = new Customer();
		
		orderCustomer.setName(CUSTOMER_NAME);
		orderCustomer.setAddress(CUSTOMER_ADDRESS);
		
		BasketItem lineItemOne = new BasketItem();
		lineItemOne.setCorrespondingMenuItem(menu.getMenuItems().get(0));
		lineItemOne.setQuantity(2);
		
		BasketItem lineItemTwo = new BasketItem();
		lineItemTwo.setCorrespondingMenuItem(menu.getMenuItems().get(1));
		lineItemTwo.setQuantity(10);
		
		Basket orderToPlace = new Basket();
		
		orderToPlace.setCustomer(orderCustomer);
		orderToPlace.getBasketItems().add(lineItemOne);
		orderToPlace.getBasketItems().add(lineItemTwo);
		
		Order placedOrder = null ;
		
		try {
			placedOrder = client.placeOrder(orderToPlace);
		}
		catch(OrderException ordEx) {
			fail("Exception thrown placing order : " + ordEx);
		}
		
		// given that the placing of the order may produce random results
		// we can only assert that the value is not null
		assertNotNull(placedOrder);
	}
}
