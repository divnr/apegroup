package com.apegroup.takeout.model;

import java.io.Serializable;

public class Customer implements Serializable {

	private static final long serialVersionUID = 7526479556423476147L;
	
	private String name;
	private String address;
	
	public Customer() {}
	
	public Customer(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
