package com.apegroup.takeout.model;

import java.io.Serializable;

public class OrderItem implements Serializable {

	private static final long serialVersionUID = 7526474556423476147L;
	
	private String id;
	
	private MenuItem correspondingMenuItem;
	
	private int quantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MenuItem getCorrespondingMenuItem() {
		return correspondingMenuItem;
	}

	public void setCorrespondingMenuItem(MenuItem correspondingMenuItem) {
		this.correspondingMenuItem = correspondingMenuItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
