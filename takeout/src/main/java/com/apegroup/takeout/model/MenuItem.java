package com.apegroup.takeout.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuItem implements Serializable {
		
	private static final long serialVersionUID = 7526481556423476147L;
	
	public static final int PIZZA = 0;
	public static final int TOPPING = 1;
	public static final int BASE = 2;
	
	private String id;
	private String name;
	
	private int type;
	
	private List<MenuItem> childItems;
	
	private double price;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<MenuItem> getChildItems() {
		return childItems;
	}

	public void setChildItems(List<MenuItem> childItems) {
		this.childItems = childItems;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public void addChildItem(MenuItem itemToAdd) {
		if(childItems==null) {
			this.childItems = new ArrayList<MenuItem>();
		}
		childItems.add(itemToAdd);
	}
}
