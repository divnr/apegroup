package com.apegroup.takeout.model;

public class OrderStatus {

	private int statusCode;
	
	private OrderStatus(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public static final OrderStatus SUCCESSFUL_ORDER = new OrderStatus(0);
	public static final OrderStatus BAD_INPUT_PARAMETER = new OrderStatus(1);
	public static final OrderStatus INTERNAL_SERVER_ERROR = new OrderStatus(2);
	public static final OrderStatus STOCK_EMPTY = new OrderStatus(3);

	public int getStatusCode() {
		return statusCode;
	}
}
