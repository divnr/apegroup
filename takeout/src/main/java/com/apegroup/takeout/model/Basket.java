package com.apegroup.takeout.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Basket implements Serializable {

	private static final long serialVersionUID = 7526471556423476147L;
	
	private int id;
	
	private Customer customer;
	
	private Map<String,BasketItem> orderedBasketItems = new Hashtable<String,BasketItem>();
	
	public Basket() {}
	
	public Basket(Customer customer) {
		this.customer = customer;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<BasketItem> getBasketItems() {
		return new ArrayList<BasketItem>(orderedBasketItems.values());
	}

	public BasketItem getBasketItem(String id) {
		return orderedBasketItems.get(id);
	}
	
	public Map<String, BasketItem> getOrderedBasketItems() {
		return orderedBasketItems;
	}

	public void setOrderedBasketItems(Map<String, BasketItem> orderedBasketItems) {
		this.orderedBasketItems = orderedBasketItems;
	}
	
	public int getTotalItems() {
		
		int totalItems = 0;
		
		for(BasketItem currentItem : this.orderedBasketItems.values()) {
			totalItems += currentItem.getQuantity();
		}
		
		return totalItems;
	}
}