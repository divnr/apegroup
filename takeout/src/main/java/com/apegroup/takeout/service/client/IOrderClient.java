package com.apegroup.takeout.service.client;


import com.apegroup.takeout.model.Customer;
import com.apegroup.takeout.model.Menu;
import com.apegroup.takeout.model.Order;
import com.apegroup.takeout.model.Basket;
import com.apegroup.takeout.service.exceptions.MenuException;
import com.apegroup.takeout.service.exceptions.OrderException;

public interface IOrderClient {

	public Menu constructMenu(Customer customer) throws MenuException;
	
	public Order placeOrder(Basket order) throws OrderException;
}
