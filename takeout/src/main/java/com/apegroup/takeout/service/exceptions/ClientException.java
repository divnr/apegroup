package com.apegroup.takeout.service.exceptions;

public class ClientException extends Throwable {

	private static final long serialVersionUID = 7826479556423476147L;

	public ClientException() {
		super();
	}

	public ClientException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClientException(String message) {
		super(message);
	}

	public ClientException(Throwable cause) {
		super(cause);
	}
}
